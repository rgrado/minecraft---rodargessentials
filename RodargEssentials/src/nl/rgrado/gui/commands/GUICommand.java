package nl.rgrado.gui.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.ui.UI;
import nl.rgrado.gui.utils.Utils;

public class GUICommand implements CommandExecutor {

	private Main plugin;
	
	public GUICommand(Main plugin) {
		this.plugin = plugin;
		
		plugin.getCommand("gui").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			return true;
		}
		
		Player p = (Player) sender;
		
		if (p.hasPermission("gui")) {
			
			if (args.length > 0) {
				if (args[0].equals("open") && p.hasPermission("gui.open")) {
					
					p.openInventory(UI.main_GUI(p));
					
				} else if (args[0].equals("give") && p.hasPermission("gui.give")) {
					
					if (args.length < 2) {
						
						ItemStack item = new ItemStack(Material.getMaterial("FEATHER"), 1);
						List<String> lore = new ArrayList();
						ItemMeta meta = item.getItemMeta();
						
						lore.add(Utils.chat("&7Click to open GUI"));
						meta.setLore(lore);
						meta.setDisplayName(Utils.chat("&eMagic Feather"));
						item.setItemMeta(meta);
						
						p.getPlayer().getInventory().addItem(item);
						p.sendMessage(Utils.chat("&7You have been given the &eMagic Feather"));
						
					} else {
						
						if (args.length < 3) {
							
							
							if (Bukkit.getServer().getPlayerExact(args[1]) != null && p.hasPermission("gui.give")) {
								
								Player player = Bukkit.getServer().getPlayerExact(args[1]); 
								
								ItemStack item = new ItemStack(Material.getMaterial("FEATHER"), 1);
								List<String> lore = new ArrayList();
								ItemMeta meta = item.getItemMeta();
								
								lore.add(Utils.chat("&7Click to open GUI"));
								meta.setLore(lore);
								meta.setDisplayName(Utils.chat("&eMagic Feather"));
								item.setItemMeta(meta);
								
								player.getPlayer().getInventory().addItem(item);
								
								if (player != p) {
									p.sendMessage(Utils.chat("&7Gave &e" + player.getName() + " &7the Magic Feather"));
								}
								
								player.sendMessage(Utils.chat("&7You have been given the &eMagic Feather"));
								
							} else {
								p.sendMessage(Utils.chat("&cPlayer not found"));
							}
							
						}
						
					}
					
				}
			} else {
				
			}
		}
		
		return false;
	}
	
}
