package nl.rgrado.gui.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.utils.Utils;

public class PlayerDeathListener implements Listener {

	
	private static Main plugin;
	
	public PlayerDeathListener(Main plugin) {
		this.plugin = plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player) {
			Player killer = e.getEntity().getKiller();
			Player p = e.getEntity();
			
			if (p != killer) {
				killer.sendMessage(Utils.chat(plugin.getConfig().getString("killed_player_message").replace("<player>", p.getDisplayName())));
				p.sendMessage(Utils.chat(plugin.getConfig().getString("player_killed_message").replace("<player>", killer.getDisplayName())));
				return;
			}
		}
	}
	
}
