package nl.rgrado.gui.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.ui.UI;
import nl.rgrado.gui.utils.Utils;

public class InventoryClickListener implements Listener {

	private Main plugin;
	
	public InventoryClickListener(Main plugin) {
		this.plugin = plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		String title = e.getView().getTitle();
		if (UI.inventorys.contains(title)) {
			e.setCancelled(true);
			if (e.getCurrentItem() == null) {
				return;
			}
			
			if (UI.inventorys.contains(title)) {
				UI.clicked((Player) e.getWhoClicked(), e.getSlot(), e.getCurrentItem(), title, e.getClickedInventory());
			}
		}
	}
	
}
