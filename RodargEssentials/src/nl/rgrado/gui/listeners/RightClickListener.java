package nl.rgrado.gui.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.ui.UI;
import nl.rgrado.gui.utils.Utils;

public class RightClickListener implements Listener {

	private static Main plugin;
	
	public RightClickListener(Main plugin) {
		this.plugin = plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPlayerRightClick(PlayerInteractEvent e) {
		
		Player p = e.getPlayer();
		
		if (e.getItem().getType() == Material.FEATHER && e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&eMagic Feather"))) {
			if (p.hasPermission("gui.open")) {
				p.openInventory(UI.main_GUI(p));
			}
		}
		
	}
	
}
