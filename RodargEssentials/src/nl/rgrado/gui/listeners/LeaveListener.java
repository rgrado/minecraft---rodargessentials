package nl.rgrado.gui.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.utils.Utils;

public class LeaveListener implements Listener {

private static Main plugin;
	
	public LeaveListener(Main plugin) {
		this.plugin = plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onJoin(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		e.setQuitMessage(
			Utils.chat(plugin.getConfig().getString("leave_message").replace("<player>", p.getName())));
	}
	
}
