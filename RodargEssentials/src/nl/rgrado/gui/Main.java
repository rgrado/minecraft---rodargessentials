package nl.rgrado.gui;

import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import nl.rgrado.gui.commands.GUICommand;
import nl.rgrado.gui.listeners.InventoryClickListener;
import nl.rgrado.gui.ui.UI;
import nl.rgrado.gui.listeners.JoinListener;
import nl.rgrado.gui.listeners.LeaveListener;
import nl.rgrado.gui.listeners.PlayerDeathListener;
import nl.rgrado.gui.listeners.RightClickListener;

public class Main extends JavaPlugin {

	@Override
	public void onEnable() {
		saveDefaultConfig();
		
		new JoinListener(this);
		new LeaveListener(this);
		new PlayerDeathListener(this);
		
		new GUICommand(this);
		
		new RightClickListener(this);
		new InventoryClickListener(this);
		UI.initialize();
	}
	
}
