package nl.rgrado.gui.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Utils {

	public static String chat(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	public static ItemStack createItem (Inventory inv, String materialId, int amount, int invSlot, String displayName, String... loreString ) {
		
		ItemStack item;
		List<String> lore = new ArrayList();
		
		item = new ItemStack(Material.getMaterial(materialId), amount);
		
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Utils.chat(displayName));
		for (String s : loreString) {
			lore.add(Utils.chat(s));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		inv.setItem(invSlot - 1, item);
		
		return item;
	}
	
	public static ItemStack getHead(Player p, String displayName, Inventory inv, int invSlot, int amount, String... loreString) {
        ItemStack item = new ItemStack(Material.LEGACY_SKULL_ITEM, amount, (short) 3);
        
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        List<String> lore = new ArrayList();
        skull.setDisplayName(displayName);
        for (String s : loreString) {
			lore.add(Utils.chat(s));
		}
        skull.setOwner(p.getName());
        skull.setLore(lore);
        item.setItemMeta(skull);
        inv.setItem(invSlot - 1, item);
        
        return item;
    }
	
	public static ItemStack getOfflineHead(OfflinePlayer p, String displayName, Inventory inv, int invSlot, int amount, String... loreString) {
        ItemStack item = new ItemStack(Material.LEGACY_SKULL_ITEM, amount, (short) 3);
        
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        List<String> lore = new ArrayList();
        skull.setDisplayName(displayName);
        for (String s : loreString) {
			lore.add(Utils.chat(s));
		}
        skull.setOwner(p.getName());
        skull.setLore(lore);
        item.setItemMeta(skull);
        inv.setItem(invSlot - 1, item);
        
        return item;
    }
	
}
