package nl.rgrado.gui.ui;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.BanList.Type;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import nl.rgrado.gui.Main;
import nl.rgrado.gui.utils.Utils;

public class UI {
	
	public static Inventory inv;
	public static ArrayList<String> inventorys;
	public static int inv_rows = 4 * 9;
	
	private static Main plugin;
	
	public UI(Main plugin) {
		this.plugin = plugin;
	}
	
	public static void initialize() {
		//Do not fuck up the order of the .add
		inventorys = new ArrayList<>();
		inventorys.add(Utils.chat("&6&lMain menu"));
		inventorys.add(Utils.chat("&6&lTeleport menu"));
		inventorys.add(Utils.chat("&6&lTeleport to player"));
		inventorys.add(Utils.chat("&6&lTeleport player to you"));
		inventorys.add(Utils.chat("&6&lGamerules"));
		inventorys.add(Utils.chat("&6&lTime menu"));
		inventorys.add(Utils.chat("&6&lBan and kick menu"));
		inventorys.add(Utils.chat("&6&lSelect player to kick"));
		inventorys.add(Utils.chat("&6&lSelect player to ban"));
		inventorys.add(Utils.chat("&6&lBan list"));
		inventorys.add(Utils.chat("&6&lSelect inventory"));
		
		inv = Bukkit.createInventory(null, inv_rows);
	}

	public static Inventory main_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(0));
		
		Utils.createItem(inv, "CLOCK", 1, 13, "&aTime", "&7Click here to see all", "&7the time and weather options");
		Utils.createItem(inv, "COMPASS", 1, 14, "&aTeleport", "&7Click here to see all the teleport options", "&bAvailable options: 3");
		Utils.createItem(inv, "BOOK", 1, 15, "&aGamerules", "&7Click here to toggle gamerules");
		Utils.createItem(inv, "IRON_BARS", 1, 22, "&aBan and kick", "&7Click here to go to the ban and kick menu");
		Utils.createItem(inv, "CHEST", 1, 23, "&aOpen inventory", "&7Click here to look into someones inventory");
		
		int onlinePlayers = Bukkit.getServer().getOnlinePlayers().size();
		if (onlinePlayers < 2) {
			Utils.getHead(p, Utils.chat("&fOnline players"), inv, 36, onlinePlayers, Utils.chat("&7There is currently &a" + onlinePlayers + " &7player online"));
		} else {
			Utils.getHead(p, Utils.chat("&fOnline players"), inv, 36, onlinePlayers, Utils.chat("&7There are currently &a" + onlinePlayers + " &7players online"));
		}
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory teleport_GUI (Player p) {
		inv = Bukkit.createInventory(null, 9);
		Inventory toReturn = Bukkit.createInventory(null, 9, inventorys.get(1));
		
		Utils.createItem(inv, "PAPER", 1, 4, "&aTeleport all", "&7Click to teleport all players to your location");
		Utils.createItem(inv, "PAPER", 1, 5, "&aTeleport to a player", "&7Click to teleport yourself to someone else");
		Utils.createItem(inv, "PAPER", 1, 6, "&aTeleport a player to you", "&7Click to teleport a player to your location");
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 9, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory teleport_to_player_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(2));
		
		int headPosition = 1;
		
		for(Player selectedPlayer : Bukkit.getOnlinePlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1);
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory teleport_player_to_you_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(3));
		
		int headPosition = 1;
		
		for(Player selectedPlayer : Bukkit.getOnlinePlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1);
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	@SuppressWarnings("deprecation")
	public static Inventory gamerule_GUI (Player p) {
		inv = Bukkit.createInventory(null, 54);
		Inventory toReturn = Bukkit.createInventory(null, 54, inventorys.get(4));
		
		String[] gameRules = p.getWorld().getGameRules();
		
		int gameruleSlot = 1;
		
		for (String gamerule : gameRules) {
			
			String changeMessage;
			String currentValueMessage;
			boolean numeric = true;
			
			try {
				Integer num = Integer.parseInt(p.getWorld().getGameRuleValue(gamerule));
			} catch (NumberFormatException e) {
				numeric = false;
			}
			
			if (!numeric) {
				
				if (!Boolean.parseBoolean(p.getWorld().getGameRuleValue(gamerule))) {
					changeMessage = "Click here to turn &aTrue";
					currentValueMessage = "&7Current value &c" + p.getWorld().getGameRuleValue(gamerule).substring(0,1).toUpperCase() + p.getWorld().getGameRuleValue(gamerule).substring(1);
				} else {
					changeMessage = "Click here to turn &cFalse";
					currentValueMessage = "&7Current value &a" + p.getWorld().getGameRuleValue(gamerule).substring(0,1).toUpperCase() + p.getWorld().getGameRuleValue(gamerule).substring(1);
				}
				
			} else {
				changeMessage = "Unable to change that here";
				currentValueMessage = "&7Current value: " + p.getWorld().getGameRuleValue(gamerule);
			}
			
			Utils.createItem(inv, "ENCHANTED_BOOK", 1, gameruleSlot, "&f" + gamerule, currentValueMessage, changeMessage);
			
			if (gameruleSlot == 9 || gameruleSlot == 27) {
				gameruleSlot += 10;
			} else {
				gameruleSlot++;
			}
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 54, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory time_GUI (Player p) {
		inv = Bukkit.createInventory(null, 9);
		Inventory toReturn = Bukkit.createInventory(null, 9, inventorys.get(5));
		
		Utils.createItem(inv, "REDSTONE", 1, 4, "&aTime set day", "&7Click here to set the time to 1000");
		Utils.createItem(inv, "GLOWSTONE_DUST", 1, 5, "&aTime set noon", "&7Click here to set the time to 6000");
		Utils.createItem(inv, "GUNPOWDER", 1, 6, "&aTime set night", "&7Click here to set the time to 13000");
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 9, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory ban_and_kick_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(6));
		
		Utils.createItem(inv, "DISPENSER", 1, 1, "&aKick player", "&7Click here to kick a player");
		Utils.createItem(inv, "TNT", 1, 19, "&aBan player", "&7Click here to ban a player");
		Utils.createItem(inv, "BOOK", 1, 20, "&aBan list", "&7Click here to see all", "&7banned players and unban players");
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory kick_player_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(7));
		
		int headPosition = 1;
		
		for(Player selectedPlayer : Bukkit.getOnlinePlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1);
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static Inventory ban_player_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(8));
		
		int headPosition = 1;
		
		for(Player selectedPlayer : Bukkit.getOnlinePlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1);
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}

	public static Inventory ban_list_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(9));
		
		Bukkit.getBannedPlayers();
		
		int headPosition = 1;
		
		for(OfflinePlayer selectedPlayer : Bukkit.getBannedPlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getOfflineHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1, "&aClick here to unban player");
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}

	public static Inventory select_inventory_GUI (Player p) {
		inv = Bukkit.createInventory(null, inv_rows);
		Inventory toReturn = Bukkit.createInventory(null, inv_rows, inventorys.get(10));
		
		Bukkit.getBannedPlayers();
		
		int headPosition = 1;
		
		for(Player selectedPlayer : Bukkit.getOnlinePlayers()){
			
			if (selectedPlayer.equals(p)) {
				continue;
			}
			
			//show all player heads
			Utils.getHead(selectedPlayer, selectedPlayer.getName(), inv, headPosition, 1, "&aClick here to show inventory");
			headPosition++;
		}
		
		//Back
		Utils.createItem(inv, "BARRIER", 1, 36, "&cBack", "&7Go back to the last page");
		
		toReturn.setContents(inv.getContents());
		return toReturn;
	}
	
	public static void clicked(Player p, int slot, ItemStack clicked, String inventoryName, Inventory inv) {
		
		//Check if the inv matches any of the available inventorys
		if (inventorys.contains(inventoryName)) {
			
			//Main inv
			if (inventoryName.equals(inventorys.get(0))) {
				
				//Clock
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTime"))) {
					p.openInventory(UI.time_GUI(p));
					
				}
				
				//Compass
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTeleport"))) {
					p.openInventory(UI.teleport_GUI(p));
					
				}
				
				//Book
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aGamerules"))) {
					p.openInventory(UI.gamerule_GUI(p));
					
				}
				
				//Iron bars
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aBan and kick"))) {
					p.openInventory(UI.ban_and_kick_GUI(p));
					
				}
				
				//Chest
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aOpen inventory"))) {
					p.openInventory(UI.select_inventory_GUI(p));
					
				}
				
			}
			
			//Teleport menu
			if (inventoryName.equals(inventorys.get(1))) {
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.main_GUI(p));
					
				}
				
				//Paper 1 -> Teleport all
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTeleport all"))) {
					
					for (Player player : Bukkit.getServer().getOnlinePlayers()) {
						
						if (player.equals(p)) {
							continue;
						}
						
					    int x = p.getLocation().getBlockX();
					    int z = p.getLocation().getBlockZ();
					    int y = p.getLocation().getBlockY();
		
					    player.teleport(new Location(p.getWorld(), x, y, z));
					    
					    player.sendMessage(Utils.chat("&7You have been teleported!"));
					}
					
					p.closeInventory();
					
				}
				
				//Paper 2 -> Teleport to player
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTeleport to a player"))) {
					
					p.openInventory(UI.teleport_to_player_GUI(p));
					
				}
				
				//Paper 3 -> Teleport player to you
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTeleport a player to you"))) {
					
					p.openInventory(UI.teleport_player_to_you_GUI(p));
					
				}
				
			}
			
			//Teleport to player
			if (inventoryName.equals(inventorys.get(2))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					Player player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
					int x = player.getLocation().getBlockX();
				    int z = player.getLocation().getBlockZ();
				    int y = player.getLocation().getBlockY();
	
				    p.teleport(new Location(p.getWorld(), x, y, z));
				    
				    p.sendMessage(Utils.chat("&7You have been teleported to &e" + clicked.getItemMeta().getDisplayName()));
				    p.closeInventory();
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.teleport_GUI(p));
					
				}
				
			}
			
			//Teleport player to you
			if (inventoryName.equals(inventorys.get(3))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					Player player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
					int x = p.getLocation().getBlockX();
				    int z = p.getLocation().getBlockZ();
				    int y = p.getLocation().getBlockY();
	
				    player.teleport(new Location(p.getWorld(), x, y, z));
				    
				    player.sendMessage(Utils.chat("&7You have been teleported to &e" + p.getDisplayName()));
				    p.closeInventory();
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.teleport_GUI(p));
					
				}
				
			}
			
			//Show gamerules
			if (inventoryName.equals(inventorys.get(4))) {
				
				if (clicked.getType().name().equalsIgnoreCase("ENCHANTED_BOOK")) {
					
					String gameRuleName = clicked.getItemMeta().getDisplayName();
					String currentGameruleValue = p.getWorld().getGameRuleValue(gameRuleName).toString();
					boolean numeric = true;
					
					try {
						Integer num = Integer.parseInt(currentGameruleValue);
					} catch (NumberFormatException e) {
						numeric = false;
					}
					
					if (!numeric) {
						
						if (Boolean.parseBoolean(p.getWorld().getGameRuleValue(gameRuleName))) {
							if(p.getWorld().setGameRuleValue(gameRuleName, "false")) {
								p.sendMessage(Utils.chat("&7Set &e" + gameRuleName + " &7to &cFalse"));
							}
						} else {
							if(p.getWorld().setGameRuleValue(gameRuleName, "true")) {
								p.sendMessage(Utils.chat("&7Set &e" + gameRuleName + " &7to &aTrue"));
							}
						}
						
						p.openInventory(UI.gamerule_GUI(p));
						
					}
					
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.main_GUI(p));
					
				}
				
			}
			
			//Show time options
			if (inventoryName.equals(inventorys.get(5))) {
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTime set day"))) {
					
					p.getWorld().setTime(1000);
					p.sendMessage(Utils.chat("&7Set time to &e1000"));
					
				}
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTime set noon"))) {
					
					p.getWorld().setTime(6000);
					p.sendMessage(Utils.chat("&7Set time to &e6000"));
				}
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aTime set night"))) {
					
					p.getWorld().setTime(13000);
					p.sendMessage(Utils.chat("&7Set time to &e13000"));
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.main_GUI(p));
					
				}
				
			}
			
			//Ban or kick menu
			if (inventoryName.equals(inventorys.get(6))) {
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aKick player"))) {
					
					p.openInventory(UI.kick_player_GUI(p));
					
				}
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aBan player"))) {
					
					p.openInventory(UI.ban_player_GUI(p));
					
				}
				
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&aBan list"))) {
					
					p.openInventory(UI.ban_list_GUI(p));
					
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.main_GUI(p));
					
				}
				
			}
			
			//Select player to kick
			if (inventoryName.equals(inventorys.get(7))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					Player player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
					player.getPlayer().kickPlayer("You have been kicked");
				    
				    p.sendMessage(Utils.chat("&7You have kicked &e" + player.getDisplayName()));
				    p.openInventory(UI.kick_player_GUI(p));
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.ban_and_kick_GUI(p));
					
				}
				
			}
			
			//Select player to ban
			if (inventoryName.equals(inventorys.get(8))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					Player player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
					Bukkit.getBanList(Type.NAME).addBan(player.getPlayer().getName(), Utils.chat("&fYour behavior"), null, null);
					player.getPlayer().kickPlayer("You have been banned");
				    
				    p.sendMessage(Utils.chat("&7You have banned &e" + player.getDisplayName()));
				    p.openInventory(UI.ban_player_GUI(p));
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.ban_and_kick_GUI(p));
					
				}
				
			}
			
			//Select player to unban
			if (inventoryName.equals(inventorys.get(9))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					OfflinePlayer player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
					Bukkit.getBanList(Type.NAME).pardon(clicked.getItemMeta().getDisplayName());
					
				    p.openInventory(UI.ban_list_GUI(p));
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.ban_and_kick_GUI(p));
					
				}
				
			}
			
			//Select player to show inventory
			if (inventoryName.equals(inventorys.get(10))) {
				
				if (clicked.getType().name().equalsIgnoreCase("PLAYER_HEAD")) {
					
					Player player = Bukkit.getPlayer(clicked.getItemMeta().getDisplayName());
					
				    p.openInventory(player.getPlayer().getInventory());
				}
				
				//Check if back button is pressed
				if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&cBack"))) {
					p.openInventory(UI.main_GUI(p));
					
				}
				
			}
			
		}
		
	}
	
}
