# RodargEssentials

## Commands
All commands and functionalities need a player with OP
* gui open (Opens the gui)
* gui give (Give yourself a Magic Feather and right click it to open the GUI)
* gui give {PlayerName} (Give the selected player a Magic Feather)

## Functionalities
* In the GUI you can change the Gamerules
* You can Kick and Ban players
* See all the banned players and Unban them in one click
* Set time of the game
* Teleport options (Tpall to your location / Tp to someones location / Tp someone to your location)
* See someones inventory

## Tutorials
I have used the tutorials on this channel

https://www.youtube.com/channel/UCTowovK5vZBR2uDwCmXwWBQ

They are very good and helped me a lot, go check them out